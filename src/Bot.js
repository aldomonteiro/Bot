import EventEmitter from 'events';
import bodyParser from 'body-parser';
import { Router } from 'express';
import Elements from './Elements.js';
import Buttons from './Buttons.js';
import QuickReplies from './QuickReplies.js';
import _ from 'lodash';
import axios from 'axios';
import fetch from './libs/fetch';

export { Elements, Buttons, QuickReplies };

const userCache = {};

export async function wait(time) {
  return new Promise(resolve => setTimeout(() => resolve(), time));
}

class Bot extends EventEmitter {
  static Buttons = Buttons;
  static Elements = Elements;

  static wait = wait;

  constructor(verification, debug) {
    super();
    this._debug = debug;
    this._verification = verification;
  }

  async deleteFields(_fields) {
    try {
      const response = await axios.delete('https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + this._token, {
        headers: { 'Content-Type': 'application/json' },
        params: {
          fields: _fields
        },
      });
      return response.result;
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.error)
        return error.response.data.error.message;
      else if (error.response.status && error.response.statusText)
        return error.response.status + ' - ' + error.response.statusText;
      else return "unknown error";
    }
  }

  async getFields(_fields) {
    try {
      const response = await axios.get('https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + this._token, {
        headers: { 'Content-Type': 'application/json' },
        params: {
          fields: JSON.stringify(_fields),
        },
      });
      if (response.data) return response.data;
      else if (response.result) return response.result;
      else return response;
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.error)
        return error.response.data.error.message;
      else if (error.response && error.response.status && error.response.statusText)
        return error.response.status + ' - ' + error.response.statusText;
      else return "unknown error";
    }
  }

  async send(to, message) {
    if (this._debug) {
      console.log({ recipient: { id: to }, message: message ? message.toJSON() : message });
    }

    try {
      await fetch('https://graph.facebook.com/v2.6/me/messages', {
        method: 'post',
        json: true,
        query: { access_token: this._token },
        body: { recipient: { id: to }, message }
      });
    } catch (e) {
      if (e.text) {
        let text = e.text;
        try {
          const err = JSON.parse(e.text).error;
          text = `${err.type || 'Unknown'}: ${err.message || 'No message'}`;
        } catch (ee) {
          // ignore
        }

        throw Error(text);
      } else {
        throw e;
      }
    }
  }

  async senderAction(to, senderAction) {
    if (this._debug) {
      console.log({ recipient: { id: to }, senderAction });
    }

    try {
      await fetch('https://graph.facebook.com/v2.6/me/messages', {
        method: 'post',
        json: true,
        query: { access_token: this._token },
        body: { recipient: { id: to }, sender_action: senderAction }
      });
    } catch (e) {
      if (e.text) {
        let text = e.text;
        try {
          const err = JSON.parse(e.text).error;
          text = `${err.type || 'Unknown'}: ${err.message || 'No message'}`;
        } catch (ee) {
          // ignore
        }

        throw Error(text);
      } else {
        throw e;
      }
    }
  }

  async setTyping(to, isTyping) {
    const senderAction = isTyping ? 'typing_on' : 'typing_off';
    this.senderAction(to, senderAction);
  }

  async startTyping(to) {
    this.setTyping(to, true);
  }

  async stopTyping(to) {
    this.setTyping(to, false);
  }

  async fetchUser(id, fields = 'first_name,last_name,profile_pic', cache = false) {
    const key = id + fields;
    let props;

    if (cache && userCache[key]) {
      props = userCache[key];
      props.fromCache = true;
    } else {
      const { body } = await fetch(`https://graph.facebook.com/v2.6/${id}`, {
        query: { access_token: this._token, fields }, json: true
      });

      props = body;
      props.fromCache = false;

      if (cache) {
        userCache[key] = props;
      }
    }

    return props;
  }

  async handleMessage(input) {
    const body = JSON.parse(JSON.stringify(input));

    // Get messaging if existis, otherwise gets standby
    const message = body.entry[0].messaging
      ? body.entry[0].messaging[0]
      : body.entry[0].standby ? body.entry[0].standby[0] : null;

    // Show message in beggning of handle message
    console.log(">>> handleMessage");
    console.log(message);
    console.log("handleMessage <<<");

    message.raw = input;

    if (message.message) {
      Object.assign(message, message.message);
      delete message.message;
    }

    message.sender.fetch = async (fields, cache) => {
      const props = await this.fetchUser(message.sender.id, fields, cache);
      Object.assign(message.sender, props);
      return message.sender;
    };

    // POSTBACK
    if (message.postback) {
      message.isButton = true;

      let postbackPayload = {};

      try {
        postbackPayload = JSON.parse(message.postback.payload);
        if (postbackPayload.hasOwnProperty('data')) {
          message.postback = postbackPayload;
          message.data = postbackPayload.data;
          message.event = postbackPayload.event;
          this.emit('postback', message.event, message, message.data);

          if (postbackPayload.hasOwnProperty('event')) {
            this.emit(message.event, message, message.data);
          }
        }
      } catch (e) {
        console.log('ERROR parsing postback.payload', postbackPayload, e);
        this.emit(message.postback.payload, message);
      }
      return;
    }

    // READ
    if (message.read) {
      this.emit('read', message, message.read);
      return;
    }

    // DELIVERY
    if (message.delivery) {
      Object.assign(message, message.delivery);
      message.delivery = message.delivery.mids;

      delete message.delivery.mids;

      this.emit('delivery', message, message.delivery);
      return;
    }

    // OPTIN
    if (message.optin) {
      message.param = message.optin.ref || true;
      message.optin = message.param;
      this.emit('optin', message, message.optin);
      return;
    }

    // QUICK_REPLY
    if (message.quick_reply && !message.is_echo) {
      let postback = {};

      try {
        postback = JSON.parse(message.quick_reply.payload) || {};
      } catch (e) {
        // ignore
      }

      message.isQuickReply = true;

      if (postback.hasOwnProperty('data')) {
        message.postback = postback;
        message.data = postback.data;
        message.event = postback.event;

        this.emit('postback', message.event, message, message.data);

        if (postback.hasOwnProperty('event')) {
          this.emit(message.event, message, message.data);
        }
      } else {
        this.emit('quick-reply', message, message.quick_reply);
      }

      return;
    }

    const attachments = _.groupBy(message.attachments, 'type');

    if (attachments.image) {
      message.images = attachments.image.map(a => a.payload.url);
    }

    if (attachments.video) {
      message.videos = attachments.video.map(a => a.payload.url);
    }

    if (attachments.audio) {
      message.audio = attachments.audio.map(a => a.payload.url)[0];
    }

    if (attachments.location) {
      const location = attachments.location[0];
      message.location = { ...location, ...location.payload.coordinates };
      delete message.location.payload;
    }

    message.object = body.object;

    delete message.attachments;

    if (this._debug)
      console.log('this.emit message', message);

    this.emit('message', message);
  }

  router() {
    const router = new Router();

    router.use(bodyParser.json());

    router.get('/', (req, res) => {
      if (req.query['hub.verify_token'] === this._verification) {
        res.send(req.query['hub.challenge']);
      } else {
        res.send('Error, wrong validation token');
      }
    });

    router.post('/', (req, res) => {
      this._token = req.token;
      this.handleMessage(req.body);
      if (this._debug) {
        console.log("bot router (req.body.entry[0])");
        console.log((req.body.entry && req.body.entry.length > 0) ? req.body.entry[0] : "received something, no body entry..");
      }
      res.send().status(200);
    });

    return router;
  }
}

export { Bot };

export default Bot;
